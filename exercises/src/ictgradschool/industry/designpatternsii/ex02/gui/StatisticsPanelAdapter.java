package ictgradschool.industry.designpatternsii.ex02.gui;

import ictgradschool.industry.designpatternsii.ex02.model.Course;
import ictgradschool.industry.designpatternsii.ex02.model.CourseListener;

public class StatisticsPanelAdapter implements CourseListener {
	private DistributionPanel sPanel;
	private Course course;

	public StatisticsPanelAdapter (Course course, DistributionPanel sPanel){
		this.course = course;
		this.sPanel = sPanel;
		course.addCourseListener(this);
	}
	@Override
	public void courseHasChanged(Course course) {
		sPanel.repaint();
	}
}
