package ictgradschool.industry.designpatternsii.ex02.gui;

import ictgradschool.industry.designpatternsii.ex02.model.Course;
import ictgradschool.industry.designpatternsii.ex02.model.CourseListener;

public class DistributionPanelAdapter implements CourseListener {
    private DistributionPanel dbPanel;
    private Course course;

    public DistributionPanelAdapter (Course course, DistributionPanel dbPanel){
		this.course = course;
		this.dbPanel = dbPanel;
        course.addCourseListener(this);
	}
	@Override
	public void courseHasChanged(Course course) {
		dbPanel.repaint();
	}
}
