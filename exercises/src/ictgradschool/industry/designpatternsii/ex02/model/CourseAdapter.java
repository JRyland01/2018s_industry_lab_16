package ictgradschool.industry.designpatternsii.ex02.model;




import javax.swing.*;
import javax.swing.table.AbstractTableModel;

public class CourseAdapter extends AbstractTableModel implements CourseListener {
	private Course course;



    public CourseAdapter (Course course){
		this.course =  course;
		course.addCourseListener(this);
	}
	@Override
	public void courseHasChanged(Course course) {
		fireTableDataChanged();
	}

    @Override
    public int getRowCount() {
        return course.size();
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
       StudentResult result = course.getResultAt(rowIndex);

       switch (columnIndex){
           case 0: return result._studentID;
           case 1: return result._studentSurname;
           case 2: return  result._studentForename;
           case 3: return result.getAssessmentElement(StudentResult.AssessmentElement.Exam);
           case 4: return result.getAssessmentElement(StudentResult.AssessmentElement.Test);
           case 5: return result.getAssessmentElement(StudentResult.AssessmentElement.Assignment);
           case 6: return  result.getAssessmentElement(StudentResult.AssessmentElement.Overall);
           default: return null;
       }
    }

    @Override
    public String getColumnName(int column) {
        switch (column){
            case 0: return "Student ID";
            case 1: return "Surname";
            case 2: return  "Forename";
            case 3: return "Exam";
            case 4: return  "Test";
            case 5: return "Assignment";
            case 6: return "Overall";
            default: return null;
        }

    }
}